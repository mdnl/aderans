<?php
namespace Deployer;

require 'recipe/common.php';
require_once '../../vendor/deployer/deployer/recipe/magento2.php';

set('default_timeout', 1960);
// Project name
set('application', 'Aderans benelux');
set('default_stage', 'production');
set('keep_releases', 2);
set('repository', 'git@bitbucket.org:mdnl/aderans.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Configuration
set('shared_files', [
    'app/etc/env.php',
    'var/.maintenance.ip',
]);

set('shared_dirs', [
    'var/log',
    'var/backups',
    'pub/media',
]);

set('writable_dirs', [
    'var',
    'pub/static',
    'pub/media',
]);

set('clear_paths', [
    'var/generation/*',
    'var/cache/*',
]);

set('allow_anonymous_stats', false);

// Hosts

host('vps01.aderansbenelux.com')
    ->user('root')
    ->stage('production')
    ->set('branch', 'master')
    ->set('deploy_path', '/home/aderans/domains/shop.aderansbenelux.com/public_html');

host('aderans.hypernode.io')
    ->user('app')
    ->stage('uat')
    ->set('branch', 'master')
    ->set('writable_mode', 'chown')
    ->set('writable_use_sudo', false)
    ->set('deploy_path', '/data/web');

task('disk_free', function() {
    $df = run('df -h /');
    writeln($df);
});

// Tasks
desc('Compile magento di');
task('magento:compile', function () {
    run("{{bin/php}} {{release_path}}/bin/magento module:enable --all");
    run("{{bin/php}} {{release_path}}/bin/magento setup:di:compile");
    run('cd {{release_path}} && {{bin/composer}} dump-autoload -o');
});

desc('Backup db');
task('magento:backup', function () {
    run('cd {{release_path}}');
    run('magerun2 db:dump -c gzip --strip="@log @sessions" var/pre-golive.tar.gz');
});

// Tasks
desc('Deploy assets');
task('magento:deploy:assets', function () {
    run("{{bin/php}} {{release_path}}/bin/magento setup:static-content:deploy nl_NL");
});


desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:magento',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
