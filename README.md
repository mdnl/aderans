# Aderans Benelux
Author: Kay in 't Veen - Microdesign B.V.

E-mail: k.veen@microdesign.nl

Last update: 12 jul 2019

## Installation
1. Clone repo
1. Create auth.json file and fill with credentials:
```
{
   "http-basic": {
      "repo.magento.com": {
         "username": "",
         "password": ""
      }
   }
}
```
1. Run `composer install`

## MacOs: Using this repository with Valet+ 🍏
1. Follow installation instructions of Valet+ 👉  [link 🔗](https://github.com/weprovide/valet-plus#installation)
1. In cloned folder run `valet link`
1. Run `valet secure` to set up ssl
1. Create a database using `valet db create`
1. Run `valet configure` root folder

## To do: Windows and hosting 📺

## Setup Magento for the first time
1. Use the following script to populate the clean database
```
bin/magento setup:install --session-save=db --cleanup-database --currency='EUR' --base-url='https://aderans.test/' --base-url-secure='https://aderans.test/' --language='nl_NL' --timezone=Europe/Amsterdam --db-host='localhost' --db-name='aderans' --db-user='root' --db-password='root' --backend-frontname='aderansAdmin' --admin-user='admin' --admin-firstname='admin' --admin-lastname='account' --admin-email='admin@email.com' --admin-password='welkom123' --use-secure-admin=1 --use-rewrites=1
```

*Important:* The code above is based on a valet+ install, if you have a different install running, adjust the variables accordingly

*Important 2:* Use an empty database for the command above!

## Start front-ending 🧙‍
This is where the ✨💫MAGIC💫✨ really starts!

1. If not installed yet, install grunt globally `npm i -g grunt-cli`
1. Rename `package.json.sample` to `package.json`
1. Rename `Gruntfile.js.sample` to `Gruntfile.js`
1. Run `npm install`
1. Create `/dev/tools/grunt/configs/local-themes.js` if not exists
1. Add the following code`
```
module.exports = {
    aderans: {
            area: 'frontend',
            name: 'Aderans/base',
            locale: 'nl_NL',
            files: [
                'css/styles-m',
                'css/styles-l',
                'css/email',
                'css/email-inline'
            ],
            dsl: 'less'
    }
}
```
1. Run `grunt exec:aderans` to republish symlinks
1. Run `grunt less:aderans` to preprocess less

## Troubleshooting
### Issues with redirecting to /server.php/
1. Run `valet db open` (open the magento database in your favorite program)
1. Go to `core_config_data` table
1. Add new row: scope: `default`, path: `web/seo/use_rewrites`, value: `1`
1. Go back to the magento cli: `bin/magento cache:clean`
1. Run `bin/magento cache:flush`
1. Now you should be able to browse around on the magento website

### 🤬 Scripts (eq. Javascript) are not loading, What is going on? 🤬
Don't panic, it's probably your cache, run `bin/magento cache:clean` and `bin/magento cache:flush` which will probably fix it. If not, do this:


![](https://media3.giphy.com/media/VS95jHa4UCOe4/giphy.gif)

### (MacOs) Composer install is missing
- Try `brew upgrade homebrew/php/php70` and `brew upgrade homebrew/php/composer`


### Deployer deployments
To deploy to production do the following
```bash
cd dev/deploy
dep deployer master
```
